import { Component } from 'react';
import axios from 'axios';
import PokemonCard from "../components/PokemonCard";
import Pagination from '../components/Pagination';

import './Home.css';

class Home extends Component {
    constructor(props) {
        super(props);

        this.state = {
            count: parseInt(809 / 25, 10) + 1,
            loading: false,
            current: 1,
            pokemons: [],
        };
    }

    componentDidMount() {
        this.fetchPokemons();
    }

    async fetchPokemons(page = 1) {
        this.setState({loading: true});

        const res = await axios.get(`//localhost:8080?page=${page}`);

        this.setState({ pokemons: res.data, loading: false });
    }

    pageChangeHandler(page) {
        if (this.state.loading || this.state.current === page) {
            return;
        }

        this.setState({current: page});
        this.fetchPokemons(page);
    }

    render() {
        return (
            <div className="Home">
                <div className="pokemons-cards">
                    {this.state.pokemons.map((p) => <PokemonCard pokemon={p} key={p.id}/>)}
                </div>
                <Pagination
                    count={this.state.count}
                    current={this.state.current}
                    onPageChange={(p) => this.pageChangeHandler(p)}
                />
            </div>
        );
    }
}

export default Home;
