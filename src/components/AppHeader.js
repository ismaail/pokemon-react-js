import { NavLink } from "react-router-dom";
import './AppHeader.css';

const AppHeader = () => {
    return (
        <header className="App-header">
            <img src="images/pokeball.png" className="App-logo" alt="" width="60"/>
            <nav>
                <NavLink exact to="/">Home</NavLink> |
                <NavLink to="/types">Type</NavLink>
            </nav>
        </header>
    );
};

export default AppHeader;
