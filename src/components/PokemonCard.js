import './PokemonCard.css';

const PokemonCard = ({pokemon}) => {
    return (
        <div className="pokemon-card">
            <h2>{pokemon.name}</h2>
            <div className="pokemon-thumbnail">
                <img src={`//localhost:8080/images/thumbnails/${pokemon.id}.png`} alt="{pokemon.name}" width="100" height="100" />
            </div>
        </div>
    );
};

export default PokemonCard;
