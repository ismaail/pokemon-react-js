import { Component } from 'react';
import './Pagination.css';

class Pagination extends Component {
    constructor(props) {
        super(props);

        this.current = props.current;
    }

    pageClickHandler(page) {
        this.current = page;
        this.props.onPageChange(page);
    }

    activeClass (page) {
        return this.current === page ? 'active' : '';
    }

    render() {
        return (
            <ul className="pagination">
                {Array(this.props.count).fill('', 0).map((v, i) => {
                    const page = i + 1;

                    return (
                        <li
                            key={i}
                            className={this.activeClass(page)}
                            onClick={() => this.pageClickHandler(page)}
                        >
                            {page}
                        </li>
                    );
                })}
            </ul>
        );
    };
}

export default Pagination;
