import { Route } from 'react-router-dom';
import Home from './views/Home';
import Types from './views/Types';
import AppHeader from "./components/AppHeader";

const App = () => {
    return (
        <div>
            <AppHeader/>
            <Route path="/" exact>
                <Home/>
            </Route>
            <Route path="/types">
                <Types/>
            </Route>
        </div>
    );
}

export default App;
